import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class FileReaderTest {
    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.Test
    public void getLineTokens() throws Exception {
        FileReader fr = new FileReader ();
        fr.setScanner("I love interviews");
        fr.setDelimeter(" ");
        String[] output = fr.getLineTokens();
        String[] expected = new String[]{"I", "love", "interviews"};
        Assert.assertArrayEquals(expected, output);
    }
}