import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {

    private Scanner innerScanner;
    private String delimeter;

    public void setScanner (String inputToScan) {
        innerScanner = new Scanner(inputToScan);
    }

    public void setDelimeter (String delimeter) {
        this.delimeter = delimeter;
    }

    public String getNextLine ()  {
        // if scanner
        if (innerScanner.hasNextLine()) {
            return innerScanner.nextLine();
        }
        return null;
    }

    public String[] getLineTokens() {
        // if delim
        String line = getNextLine();

        if ( line != null) {
           // String delims = "[ ]+";
            String[] tokens = line.split(this.delimeter);
            return tokens;
//            if (tokens.length > 1)
//                System.out.println(tokens[1]);
        }
        return null;
    }

    public StringBuffer readFile (String filepath) throws FileNotFoundException {
        StringBuffer output = new StringBuffer("");
        File myFile = new File(filepath);
        Scanner sc = new Scanner(myFile);

        while ( sc.hasNextLine()){
            String line = sc.nextLine();
            output.append(line);
        }

        return output;
    }
}
